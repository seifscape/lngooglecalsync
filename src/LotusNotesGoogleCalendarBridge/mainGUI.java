// Copyright 2009 Shin Sterneck, Dean Hill
//
// This file is part of the Lotus Notes to Google Calendar Synchronizer application.
//
//    This application is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This application is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this application.  If not, see <http://www.gnu.org/licenses/>.


package LotusNotesGoogleCalendarBridge;

import LotusNotesGoogleCalendarBridge.ProxyModule.ProxyConfigBean;
import LotusNotesGoogleCalendarBridge.LotusNotesService.LotusNotesExport;
import LotusNotesGoogleCalendarBridge.LotusNotesService.NotesCalendarEntry;
import LotusNotesGoogleCalendarBridge.GoogleService.GoogleImport;

import com.google.gdata.data.calendar.CalendarEventEntry;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.math.BigDecimal;
import preferences.ConfigurationBean;

public class mainGUI {

    public mainGUI() {
        // Initialize proxy bean
        proxy = new ProxyConfigBean();

        // Load configuration bean
        confBean = new ConfigurationBean();
        confBean.readConfig();

        loadSettings();

        setDateRange();
    }

    public static void main(String args[]) {
        // Run in "silent" command-line mode
        // Use all configuration settings from the property file
        new mainGUI().runCommandLine();
        System.exit(exitCode);
    }
    
    /**
     * Runs the synchronization in silent mode using existing configuration settings.
     */
    public void runCommandLine(){
        try {
            // Make sure the GUI is hidden
            isSilentMode = true;
            doSync();
        } catch (Exception ex) {
            exitCode = EXIT_EXCEPTION;
            System.out.println("\nGeneral problem: " + ex.getMessage());
            ex.printStackTrace();
        }
    }


    /**
     * Perform synchronization independent of GUI or non-GUI mode.
     */
    public void doSync() throws Exception {
        long startTime = System.currentTimeMillis();

        proxy.deactivateNow();

        statusClear();

        if (confBean.getSyncOnStartup())
            statusAppendLine("Automatic sync-on-startup is enabled. Starting sync.");
        else
            statusAppendLine("Starting sync");
        
        if (confBean.getDiagnosticMode()) {
            // Don't echo the commented out values for privacy reasons
            statusAppendLineDiag("Application Version: " + appVersion);
            statusAppendLineDiag("OS: " + System.getProperty("os.name") + " " + System.getProperty("os.version") + " " + System.getProperty("os.arch"));
            statusAppendLineDiag("Java: " + System.getProperty("java.version") + " " + System.getProperty("java.vendor"));
            //statusAppendLineDiag("Lotus Username: " + confBean.getLotusNotesUsername());
            statusAppendLineDiag("Local Server: " + confBean.getLotusNotesServerIsLocal());
            //statusAppendLineDiag("Server: " + confBean.getLotusNotesServer());
            //statusAppendLineDiag("Mail File: " + confBean.getLotusNotesMailFile());
            //statusAppendLineDiag("Google Email: " + confBean.getGoogleUserName());
            statusAppendLineDiag("Use Proxy: " + confBean.getGoogleEnableProxy());
            statusAppendLineDiag("Use SSL: " + confBean.getGoogleUseSSL());
            statusAppendLineDiag("Sync Description: " + confBean.getSyncDescription());
            statusAppendLineDiag("Sync Alarms: " + confBean.getSyncAlarms());
            statusAppendLineDiag("Sync Days In Future: " + confBean.getSyncDaysInFuture());
            statusAppendLineDiag("Sync Days In Past: " + confBean.getSyncDaysInPast());
            statusAppendLineDiag("Java Classpath: " + System.getProperty("java.class.path"));
            statusAppendLineDiag("Java Library Path: " + System.getProperty("java.library.path"));
        }

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        statusAppendLine("Date range: " + df.format(minStartDate) + " thru " + df.format(maxEndDate));

        // === Get the Lotus Notes calendar data
        statusAppendStart("Getting Lotus Notes calendar entries");
        LotusNotesExport lotusNotesService = new LotusNotesExport();

        lotusNotesService.setRequiresAuth(true);
        lotusNotesService.setCredentials(confBean.getLotusNotesUsername(), confBean.getLotusNotesPassword());
        String lnServer = confBean.getLotusNotesServer();
        if (confBean.getLotusNotesServerIsLocal())
            lnServer = "";
        lotusNotesService.setServer(lnServer);
        lotusNotesService.setMailFile(confBean.getLotusNotesMailFile());
        lotusNotesService.setMinStartDate(minStartDate);
        lotusNotesService.setMaxEndDate(maxEndDate);
        lotusNotesService.setDiagnosticMode(confBean.getDiagnosticMode());
        
        ArrayList<NotesCalendarEntry> lotusCalEntries = lotusNotesService.getCalendarEntries();
        statusAppendFinished();
        statusAppendLine("  " + lotusCalEntries.size() + " entries found within date range");
        if (confBean.getDiagnosticMode())
            statusAppendLineDiag("Lotus Version: " + lotusNotesService.getNotesVersion());

//if (true) {statusAppendLineDiag("DEBUG: Lotus Notes tasks finished. Stopping sync."); return;}

        // === Copy the Lotus Notes data to Google calendar

        if (confBean.getGoogleEnableProxy()) {
            if (! confBean.getGoogleProxyUsername().isEmpty()) {
                proxy.enableProxyAuthentication(true);
                proxy.setProxyUser(confBean.getGoogleProxyUsername());
                proxy.setProxyPassword(confBean.getGoogleProxyPassword());
            }
            
            proxy.activateNow();
        }




        // check whether the user has deselected to use SSL when connecting to google (this is not recommended)
        statusAppendStart("Logging into Google");
        GoogleImport googleService = new GoogleImport(confBean.getGoogleUserName(), confBean.getGooglePassword(), confBean.getGoogleCalendarName(), confBean.getGoogleUseSSL());
        statusAppendFinished();
//if (true) {statusAppendLineDiag("DEBUG: Done logging into Google. Stopping sync."); return;}

        googleService.setDiagnosticMode(confBean.getDiagnosticMode());
        googleService.setSyncDescription(confBean.getSyncDescription());
        googleService.setSyncAlarms(confBean.getSyncAlarms());
        googleService.setSyncMeetingAttendees(confBean.getSyncMeetingAttendees());
        googleService.setMinStartDate(minStartDate);
        googleService.setMaxEndDate(maxEndDate);

        statusAppendStart("Getting Google calendar entries");
        ArrayList<CalendarEventEntry> googleCalEntries = googleService.getCalendarEntries();
        statusAppendFinished();
        statusAppendLine("  " + googleCalEntries.size() + " entries found within date range");

        statusAppendStart("Comparing Lotus Notes and Google calendar entries");
        googleService.compareCalendarEntries(lotusCalEntries, googleCalEntries);
        statusAppendFinished();
        statusAppendLine("  " + lotusCalEntries.size() + " entries to create. " + googleCalEntries.size() + " entries to delete.");

//googleService.createSampleGEntry();
//if (true) {statusAppendLineDiag("DEBUG: Done comparing entries. Stopping sync."); return;}

        if (googleCalEntries.size() > 0) {
            statusAppendStart("Deleting old Google calendar entries");
            int deleteCount = googleService.deleteCalendarEntries(googleCalEntries);
            statusAppendFinished();
            statusAppendLine("  " + deleteCount + " entries deleted");
        }

        if (lotusCalEntries.size() > 0) {
            statusAppendStart("Creating new Google calendar entries");
            int createdCount = 0;
            createdCount = googleService.createCalendarEntries(lotusCalEntries);
            statusAppendFinished();
            statusAppendLine("  " + createdCount + " entries created");
        }

        long elapsedMillis = System.currentTimeMillis() - startTime;    
        BigDecimal elapsedSecs = new BigDecimal(elapsedMillis / 1000.0).setScale(1, BigDecimal.ROUND_HALF_UP);
        statusAppendLine("Finished sync (" + elapsedSecs + " s total)");            	
    }
   

    private void loadSettings() {
        try {
            // Configure proxy settings
            proxy.setProxyHost(confBean.getGoogleProxyIP());
            proxy.setProxyPort(confBean.getGoogleProxyPort());
            proxy.setEnabled(confBean.getGoogleEnableProxy());
        } catch (Exception ex) {
            statusAppendException("Unable to read settings from the config file.", ex);
        }
    }


    protected void setDateRange() {
        // Define our min start date for entries we will process
        Calendar now = Calendar.getInstance();
        int syncDaysInPast = confBean.getSyncDaysInPast() * -1;
        now.add(Calendar.DATE, syncDaysInPast);
        // Clear out the time portion
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        minStartDate = now.getTime();

        // Define our max end date for entries we will process
        now = Calendar.getInstance();
        int syncDaysInFuture = 0;
        syncDaysInFuture = confBean.getSyncDaysInFuture();
        now.add(Calendar.DATE, syncDaysInFuture);
        // Set the time portion
        now.set(Calendar.HOUR_OF_DAY, 23);
        now.set(Calendar.MINUTE, 59);
        now.set(Calendar.SECOND, 59);
        maxEndDate = now.getTime();
    }

    /**
     * Clears the status text area.
     */
    protected void statusClear() {
    }
            
    /**
     * Adds a line to the status area.
     * @param text - The text to add.
     */
    protected void statusAppendLine(String text) {
        System.out.println(text);
    }

    /**
     * Adds a line to the status area in diagnostic format.
     * @param text - The text to add.
     */
    protected void statusAppendLineDiag(String text) {
    	statusAppendLine("    " + text);
    }

    protected void statusAppendStart(String text) {
        statusStartTime = System.currentTimeMillis();
        System.out.print(text);
    }

    protected void statusAppendFinished() {
        // Convert milliseonds to seconds and round to the tenths place
        long elapsedMillis = System.currentTimeMillis() - statusStartTime;
        BigDecimal elapsedSecs = new BigDecimal(elapsedMillis / 1000.0).setScale(1, BigDecimal.ROUND_HALF_UP);
        statusAppendLine(" (" + elapsedSecs.toString() + " s)");
    }

    protected void statusAppendException(String msg, Exception ex) {
        statusAppendLine("\n\n=== ERROR ===");
        statusAppendLine(msg);

        // Add the stack trace to the status area
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        statusAppendLine(sw.toString());
    }

    ProxyConfigBean proxy;
    SyncCompletedDialog syncCompletedDialog;
    ConfigurationBean confBean;
    private boolean isUrlValid = false;
    long statusStartTime = 0;
    // An exit code of 0 is success. All other values are failure.
    final String appVersion = "1.10";
    private boolean isSilentMode = false;
    private boolean saveSettingsOnExit = true;

    // Our min and max dates for entries we will process.
    // If the calendar entry is outside this range, it is ignored.
    Date minStartDate = null;
    Date maxEndDate = null;

    static final int EXIT_SUCCESS = 0;
    static final int EXIT_INVALID_PARM = 1;
    static final int EXIT_EXCEPTION = 2;
    static int exitCode = EXIT_SUCCESS;
}

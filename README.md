Lotus Notes -- Google Calendar Synchronizer (headless fork)
===========================================================

What is this?
-------------

This is a fork of the
[lngooglecalsync](http://sourceforge.net/projects/lngooglecalsync/)
project.  The original code opens a GUI even when it is run in silent
mode. This can become annoying when all you want is to run it silently
from your `crontab`, since it pops up a menu bar that steals focus from
the currently running app.

Why?
----

The original code tightly couples the GUI to the rest of the code. I
took the easy way out and removed the GUI altogether and run it
completely headless.
